<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Acceuil</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amaranth:400,400i,700,700i">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/untitled.css">
</head>

<body>
    <?php include"assets/other/nav1.php";?>

    <header class="masthead" style="background-image:url('assets/img/616945.png');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 mx-auto">
                    <div class="site-heading">
                        <h1 style="font-family: Amaranth, sans-serif;"><u>This is The Blog</u></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8">
                <?php
                  if(isset($_POST['button'])||!isset($_POST['button'])){
                    $database=new PDO("mysql:host=localhost;dbname=data_db","dayanechronos","cortana4002");
                    $request_1=$database->query("select * from article order by id_article desc");
                    $_SESSION["reci"]=$request_1;
                    while($art=$_SESSION["reci"]->fetch())  {
                ?>
                    <div class="post-preview" style="font-family: Lora, sans-serif;">
                        <h2 class="post-title"><?=($art["titre"])?></h2>
                        <h4 class="post-subtitle"><?=($art["soustitre"])?></h4>
                    <p class="post-meta">Posted by <?= $_SESSION['user']?> ;Start on <?=($art["date_article"])?></p>
                </div>
                <form action="" method="post">
                 <div class="btn-group" role="group" style="width: 200px;">
                    <button class="btn btn-primary buton1" type="submit" style="background-color: blue;height:50px;"><a href="?idM=<?=$art['id_article']?>">Voir+</a></button>
                    <button class="btn btn-primary buton1" type="submit" style="margin-left: 10%;background-color: green;height:50px;" name="bouton2">Modifier</button>
                    <button class="btn btn-primary buton1" type="submit" style="margin-left: 10%;background-color: red;height:50px;" name="bouton3">Supprimer</button>
                </div>
                </form>
                <hr>
                <?php 
                }
                }
                ?>
            </div>
        </div>
    </div>
    <?php include"assets/other/footer.php";?> 

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/clean-blog.js"></script>
    <script src="assets/js/popup.js"></script>
    <script src="assets/js/Profile-Edit-Form.js"></script>
</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Blog Post - Brand</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amaranth:400,400i,700,700i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
</head>

<body>
    <?php include"assets/other/nav.php";?>


 <header class="masthead" style="background-image:url('./upload/<?=$_SESSION["image"]?>');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 mx-auto">
                    <div class="post-heading">
                        <h1><?=($_SESSION["titre"]);?></h1>
                        <h2 class="subheading"><?=($_SESSION["sutitre"]);?></h2><span class="meta">Posted by&nbsp;<a href="/home"><?=($_SESSION["user"]);?></a>&nbsp;on <?=($_SESSION["date"]);?></span></div>
                </div>
            </div>
        </div>
    </header>
    <article>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 mx-auto" style="font-family: Amaranth, sans-serif;">
                    <?=($_SESSION["contenu"]);?>
                </div>
            </div>
        </div>
    </article>
    
    <?php include"assets/other/footer.php";?> 
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/clean-blog.js"></script>
</body>

</html>
<?php
    session_start();
    

    $requete=strtolower($_SERVER['PATH_INFO']);

    if ($requete=='/subscribe') {
        require_once('../Controller/InscriptionController');
    }
    elseif ($requete=='/login') {
        require_once('../Controller/connectionController');
    }
    elseif ($requete=='/index') {
        require_once('../View/index.php');
    }
    elseif ($requete=='/home') {
        require_once('../Controller/homeController');
    }
    elseif ($requete=='/post') {
        require_once('../Controller/postController');
    }
    elseif ($requete=='/about') {
        require_once('../View/about.php');
    }
    elseif ($requete=='/contact') {
        require_once('../View/contact.php');
    }
    else {
        echo("Pages en cours");
    }